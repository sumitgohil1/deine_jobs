import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController
    with GetSingleTickerProviderStateMixin {
  Rx<int> selectedIndex = 0.obs;

  Rx<bool> isLoginHovered = false.obs;

  Rx<bool> isRegisterButtonVisible = false.obs;

  late TabController tabController;

  @override
  void onInit() {
    tabController = TabController(length: 3, vsync: this);

    tabController.addListener(() {
      if (!tabController.indexIsChanging) {
        selectedIndex.value = tabController.index;
      }
    });
    super.onInit();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }
}
