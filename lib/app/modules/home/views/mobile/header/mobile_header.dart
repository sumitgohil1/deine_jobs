import 'package:deine_jobs/utils/constants/asset_constants.dart';
import 'package:deine_jobs/utils/extensions/mobile_size.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gradient_like_css/gradient_like_css.dart';
import 'package:flutter_svg/svg.dart';

class MobileHeader extends StatelessWidget {
  const MobileHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        CustomPaint(
          size: Size(360.getWidthMobile(context.width),
              660.getHeightMobile(context.height * .9)),
          painter: MobileHeaderBackgroundCurve(),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 90,
            ),
            Container(
              alignment: Alignment.center,
              height: 100.getHeightMobile(context.height),
              width: 320.getWidthMobile(context.width),
              child: Text(
                "Deine Job website",
                maxLines: 2,
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                  fontWeight: FontWeight.w400,
                  fontSize: 42.getHeightMobile(context.height),
                  color: const Color(0xff2D3748),
                ),
              ),
            ),
            SvgPicture.asset(
              undraw_agreement_aajr,
              // fit: BoxFit.cover,

              height: 360.getHeightMobile(context.height),
              width: 404.getWidthMobile(context.width),
            )
          ],
        )
      ],
    );
  }
}

class MobileHeaderBackgroundCurve extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    // Layer 1

    Paint paintFill0 = Paint()
      ..color = const Color.fromARGB(255, 255, 255, 255)
      ..style = PaintingStyle.fill
      ..strokeWidth = size.width * 0.00
      ..strokeCap = StrokeCap.butt
      ..strokeJoin = StrokeJoin.miter;
    paintFill0.shader = linearGradient(141, ["#EBF4FF", "#E6FFFA"])
        .createShader(Rect.fromCenter(
            center: Offset(size.width / 2, size.height / 2),
            width: size.width,
            height: size.height));

    Path path_0 = Path();
    path_0.moveTo(0, 0);
    path_0.quadraticBezierTo(size.width * -0.0162500, size.height * 0.7439062,
        size.width * -0.0161111, size.height * 0.9918750);
    path_0.cubicTo(
        size.width * 0.1913056,
        size.height * 1.0368750,
        size.width * 0.7357500,
        size.height * 0.8282344,
        size.width * 1.0037778,
        size.height * 0.8785000);
    path_0.quadraticBezierTo(size.width * 1.0037778, size.height * 0.7031094,
        size.width, size.height * 0.0031250);

    canvas.drawPath(path_0, paintFill0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
