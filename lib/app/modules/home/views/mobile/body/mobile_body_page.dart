import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../../../utils/constants/colors_constant.dart';
import '../../../../../../widgets/background_curve_mobile.dart';

class MobilePage extends StatelessWidget {
  final String bodyTitle, title1, title2, title3;
  final String asset1, asset2, asset3;
  final double asset1Right, asset2Right, asset3Right;
  final double asset1Top, asset2Top, asset3Top;
  final Size asset1Size, asset2Size, asset3Size;

  const MobilePage(
      {super.key,
      required this.bodyTitle,
      required this.title1,
      required this.title2,
      required this.title3,
      required this.asset1,
      required this.asset2,
      required this.asset3,
      required this.asset1Right,
      required this.asset2Right,
      required this.asset3Right,
      required this.asset1Top,
      required this.asset2Top,
      required this.asset3Top,
      required this.asset1Size,
      required this.asset2Size,
      required this.asset3Size});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          // height: 50.h,
          width: 280.w,
          child: Text(
            bodyTitle,
            textAlign: TextAlign.center,
            style: GoogleFonts.lato(
              fontSize: 21.sp,
              color: color_4A5568,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        SizedBox(
          height: 20.h,
        ),
        SizedBox(
          height: 1043.h,
          child: Stack(
            children: [
              Positioned(
                top: 579.h,
                right: 110.w,
                child: Container(
                  height: 303.h,
                  width: 303.w,
                  padding: EdgeInsets.only(left: 98.w, top: 29.h),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: color_F7FAFC.withOpacity(.5),
                  ),
                  child: SizedBox(
                    width: 102.w,
                    height: 156.h,
                    child: Text(
                      "3.",
                      style: GoogleFonts.lato(
                        fontSize: 130.h,
                        color: const Color(0xff718096),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 648.h,
                right: 36.w,
                child: SizedBox(
                  // height: 19.h,
                  width: 143.w,
                  child: Text(
                    title3,
                    style: GoogleFonts.lato(
                      color: const Color(0xff718096),
                      fontSize: 19.sp,
                    ),
                  ),
                ),
              ),
              Positioned(
                top: asset3Top,
                right: asset3Right,
                child: SizedBox(
                  height: asset1Size.height,
                  width: asset1Size.width,
                  child: SvgPicture.asset(
                    asset3,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
              Positioned(
                top: 257.h,
                child: CustomPaint(
                  size: Size(context.width, context.height * .6),
                  painter: BackgroundCurveMobile(),
                ),
              ),
              Positioned(
                top: 284.h,
                left: 37.w,
                child: SizedBox(
                  height: 156.h,
                  width: 105.w,
                  child: Text(
                    "2.",
                    style: GoogleFonts.lato(
                      fontSize: 130.spMin,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 370.h,
                right: 14.w,
                child: SizedBox(
                  // height: 19.h,
                  width: 184.w,
                  child: Text(
                    title2,
                    style: GoogleFonts.lato(
                      color: const Color(0xff718096),
                      fontSize: 19.sp,
                    ),
                  ),
                ),
              ),
              Positioned(
                top: asset2Top,
                right: asset2Right,
                child: SizedBox(
                  height: asset2Size.height,
                  width: asset2Size.width,
                  child: SvgPicture.asset(
                    asset2,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Positioned(
                top: 77.h,
                right: 186.w,
                child: Container(
                  height: 208.h,
                  width: 208.w,
                  padding: EdgeInsets.only(left: 30.w, top: 29.h),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: color_F7FAFC.withOpacity(.5),
                  ),
                  child: Text(
                    "1.",
                    style: GoogleFonts.lato(
                      fontSize: 130.h,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 216.h,
                right: 39.w,
                child: SizedBox(
                  // height: 19.h,
                  width: 184.w,
                  child: Text(
                    title1,
                    style: GoogleFonts.lato(
                      color: const Color(0xff718096),
                      fontSize: 19.sp,
                    ),
                  ),
                ),
              ),
              Positioned(
                top: asset1Top,
                right: asset1Right,
                child: SizedBox(
                  height: asset3Size.height,
                  width: asset3Size.width,
                  child: SvgPicture.asset(
                    asset1,
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
