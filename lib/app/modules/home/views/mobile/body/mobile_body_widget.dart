import 'package:deine_jobs/app/modules/home/controllers/home_controller.dart';
import 'package:deine_jobs/utils/constants/asset_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../../widgets/custom_tabs_desktop_widget.dart';
import 'mobile_body_page.dart';

class MobileBodyWidget extends GetView<HomeController> {
  const MobileBodyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 27.h,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 17.w),
          child: const CustomTabs(),
        ),
        SizedBox(
          height: 30.h,
        ),
        Obx(() {
          if (controller.selectedIndex.value == 0) {
            return MobilePage(
              bodyTitle: "Drei einfache Schritte zu deinem neuen Job",
              title1: "Erstellen dein Lebenslauf",
              title2: "Erstellen dein Lebenslauf",
              title3: "Mit nur einem Klick bewerben",
              asset1: undraw_Profile_data_re_v81r_mobile,
              asset2: undraw_task_31wc_mobile,
              asset3: undraw_personal_file_222m_mobile,
              asset1Top: 20.h,
              asset1Right: 40.w,
              asset2Top: 436.h,
              asset2Right: 67.w,
              asset3Top: 695.h,
              asset3Right: 7.w,
              asset1Size: Size(281.w, 210.h),
              asset2Size: Size(180.w, 130.h),
              asset3Size: Size(220.w, 145.h),
            );
          } else if (controller.selectedIndex.value == 1) {
            return MobilePage(
              bodyTitle: "Drei einfache Schritte zu deinem neuen Mitarbeiter",
              title1: "Erstellen dein Unternehmensprofil",
              title2: "Erstellen ein Jobinserat",
              title3: "Wähle deinen neuen Mitarbeiter aus",
              asset1: undraw_Profile_data_re_v81r_mobile,
              asset2: undraw_about_me_wa29_mobile,
              asset3: undraw_swipe_profiles1_i6mr_mobile,
              asset1Top: 20.h,
              asset1Right: 40.w,
              asset2Top: 388.h,
              asset2Right: 42.w,
              asset3Top: 752.h,
              asset3Right: 60.w,
              asset1Size: Size(281.w, 210.h),
              asset2Size: Size(258.w, 179.h),
              asset3Size: Size(240.w, 196.h),
            );
          } else if (controller.selectedIndex.value == 2) {
            return MobilePage(
              bodyTitle:
                  "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter",
              title1: "Erstellen dein Unternehmensprofil",
              title2: "Erhalte Vermittlungs- angebot von Arbeitgeber",
              title3: "Vermittlung nach Provision oder Stundenlohn",
              asset1: undraw_Profile_data_re_v81r_mobile,
              asset2: undraw_job_offers_kw5d_mobile,
              asset3: undraw_business_deal_cpi9_mobile,
              asset1Top: 20.h,
              asset1Right: 40.w,
              asset2Top: 427.h,
              asset2Right: 40.w,
              asset3Top: 739.h,
              asset3Right: 55.w,
              asset1Size: Size(281.w, 210.h),
              asset2Size: Size(217.w, 147.h),
              asset3Size: Size(250.w, 190.h),
            );
          }

          return SizedBox();
        }),
        const SizedBox(
          height: 90,
        ),
      ],
    );
  }
}
