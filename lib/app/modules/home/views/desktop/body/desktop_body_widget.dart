import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../../../utils/constants/asset_constants.dart';
import '../../../../../../widgets/custom_tabs_desktop_widget.dart';
import '../../../controllers/home_controller.dart';
import 'page/dekstop_page.dart';

class DesktopBodyWidget extends GetView<HomeController> {
  const DesktopBodyWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 35,
        ),
        const CustomTabs(),
        const SizedBox(
          height: 55,
        ),
        Obx(() {
          if (controller.selectedIndex.value == 0) {
            return const DesktopPage(
              bodyTitle: "Drei einfache Schritte zu deinem neuen Job",
              title1: "Erstellen dein Lebenslauf",
              title2: "Erstellen dein Lebenslauf",
              title3: "Mit nur einem Klick bewerben",
              asset1: undraw_Profile_data_re_v81r,
              asset2: undraw_task_31wc,
              asset3: undraw_personal_file_222m,
            );
          } else if (controller.selectedIndex.value == 1) {
            return const DesktopPage(
              bodyTitle: "Drei einfache Schritte zu deinem neuen Mitarbeiter",
              title1: "Erstellen dein Unternehmensprofil",
              title2: "Erstellen ein Jobinserat",
              title3: "Wähle deinen neuen Mitarbeiter aus",
              asset1: undraw_Profile_data_re_v81r,
              asset2: undraw_about_me_wa29,
              asset3: undraw_swipe_profiles1_i6mr,
            );
          } else if (controller.selectedIndex.value == 2) {
            return const DesktopPage(
              bodyTitle: "Drei einfache Schritte zu deinem neuen Job",
              title1: "Erstellen dein Lebenslauf",
              title2: "Erstellen dein Lebenslauf",
              title3: "Mit nur einem Klick bewerben",
              asset1: undraw_Profile_data_re_v81r,
              asset2: undraw_job_offers_kw5d,
              asset3: undraw_business_deal_cpi9,
            );
          }

          return const SizedBox();
        })
      ],
    );
  }
}
