import 'package:deine_jobs/utils/extensions/desktop_size.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../../../../utils/constants/asset_constants.dart';
import '../../../../../../../utils/constants/colors_constant.dart';
import '../../../../../../../widgets/background_curve_desktop.dart';
import '../../../../controllers/home_controller.dart';

class DesktopPage extends GetView<HomeController> {
  final String bodyTitle, title1, title2, title3;
  final String asset1, asset2, asset3;

  const DesktopPage(
      {super.key,
      required this.bodyTitle,
      required this.title1,
      required this.title2,
      required this.title3,
      required this.asset1,
      required this.asset2,
      required this.asset3});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 96.getHeight(context.width),
          width: 390.getWidth(context.width),
          child: SelectableText(
            bodyTitle,
            textAlign: TextAlign.center,
            style: GoogleFonts.lato(
              color: color_4A5568,
              fontSize: 40.getHeight(
                context.width,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 1617.getHeight(context.width),
          child: Stack(
            children: [
              Positioned(
                top: 133.getHeight(context.width),
                left: 297.getWidth(context.width),
                child: Container(
                  height: 208.getHeight(context.width),
                  width: 208.getWidth(context.width),
                  padding: EdgeInsets.only(
                      left: 46.getWidth(context.width),
                      top: 26.getHeight(context.width)),
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: color_F7FAFC,
                  ),
                  child: Text(
                    "1.",
                    style: GoogleFonts.lato(
                      fontSize: (130 * context.width) / 1920,
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 240.getHeight(context.width),
                left: 468.getWidth(context.width),
                child: SizedBox(
                  height: 36.getHeight(context.width),
                  width: 351.getWidth(context.width),
                  child: SelectableText(
                    title1,
                    style: GoogleFonts.lato(
                      color: const Color(0xff718096),
                      fontSize: 30.getHeight(context.width),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 98.getHeight(context.width),
                left: 880.getWidth(context.width),
                child: SizedBox(
                  height: 253.getHeight(context.width),
                  width: 384.getWidth(context.width),
                  child: SvgPicture.asset(
                    asset1,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Positioned(
                top: 541.getHeight(context.width),
                left: 0,
                child: CustomPaint(
                  size: Size(context.width,
                      (Get.width * 0.19103678464640847).toDouble()),
                  painter: BackgroundCurveDesktop(),
                ),
              ),
              Positioned(
                top: 634.getHeight(context.width),
                left: 557.getWidth(context.width),
                child: SizedBox(
                  height: 227.getHeight(context.width),
                  width: 324.getWidth(context.width),
                  child: SvgPicture.asset(
                    asset2,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Positioned(
                top: 690.getHeight(context.width),
                left: 1004.getWidth(context.width),
                child: Text(
                  "2.",
                  style: GoogleFonts.lato(
                    fontSize: (130 * context.width) / 1920,
                    color: const Color(0xff718096),
                  ),
                ),
              ),
              Positioned(
                top: 788.getHeight(context.width),
                left: 1129.getWidth(context.width),
                child: SizedBox(
                  height: 36.getHeight(context.width),
                  width: 352.getWidth(context.width),
                  child: SelectableText(
                    title2,
                    style: GoogleFonts.lato(
                      color: const Color(0xff718096),
                      fontSize: 30.getHeight(context.width),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 1083.getHeight(context.width),
                left: 465.getWidth(context.width),
                child: Container(
                  height: 304.getHeight(context.width),
                  width: 303.getWidth(context.width),
                  padding: EdgeInsets.only(
                    left: 110.getWidth(context.width),
                  ),
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: color_F7FAFC,
                  ),
                  child: Text(
                    "3.",
                    style: GoogleFonts.lato(
                      fontSize: 130.getWidth(context.width),
                      color: const Color(0xff718096),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 1138.getHeight(context.width),
                left: 700.getWidth(context.width),
                child: SizedBox(
                  height: 72.getHeight(context.width),
                  width: 275.getWidth(context.width),
                  child: SelectableText(
                    title3,
                    minLines: 2,
                    style: GoogleFonts.lato(
                      color: const Color(0xff718096),
                      fontSize: 30.getHeight(context.width),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 1007.getHeight(context.width),
                left: 1002.getWidth(context.width),
                child: SizedBox(
                  height: 375.getHeight(context.width),
                  width: 502.getWidth(context.width),
                  child: SvgPicture.asset(
                    asset3,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Positioned(
                top: 322.getHeight(context.width),
                left: 447.getWidth(context.width),
                child: SizedBox(
                  height: 387.getHeight(context.width),
                  width: 583.getWidth(context.width),
                  child: SvgPicture.asset(
                    arrow1,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Positioned(
                top: 867.getHeight(context.width),
                left: 617.getWidth(context.width),
                child: SizedBox(
                  height: 234.getHeight(context.width),
                  width: 530.getWidth(context.width),
                  child: SvgPicture.asset(
                    arrow2,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
