import 'package:deine_jobs/utils/extensions/desktop_size.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gradient_like_css/gradient_like_css.dart';
import 'package:visibility_detector/visibility_detector.dart';

import '../../../../../../utils/constants/asset_constants.dart';
import '../../../../../../widgets/registration_button.dart';
import '../../../controllers/home_controller.dart';

class DesktopHeader extends GetView<HomeController> {
  const DesktopHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CustomPaint(
          size: Size(context.width, context.width * .34),
          painter: DesktopHeaderBackgroundCurve(),
        ),
        Container(
          margin: EdgeInsets.only(top: 1080 * .1, right: context.width * .25),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    height: 156.getHeight(context.width),
                    width: 320.getWidth(context.width),
                    child: Text(
                      "Deine Job website",
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.lato(
                        fontWeight: FontWeight.w700,
                        fontSize: 65.getWidth(context.width),
                        color: const Color(0xff2D3748),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 53.getHeight(context.width),
                  ),
                  VisibilityDetector(
                    key: const Key('desktop-register'),
                    onVisibilityChanged: (VisibilityInfo info) {
                      var visiblePercentage = info.visibleFraction * 100;
                      if (visiblePercentage != 100) {
                        controller.isRegisterButtonVisible.value = true;
                      } else {
                        controller.isRegisterButtonVisible.value = false;
                      }
                    },
                    child: RegustrationButton(
                      width: 320.getWidth(context.width),
                    ),
                  )
                ],
              ),
              const SizedBox(
                width: 144,
              ),
              Container(
                clipBehavior: Clip.antiAlias,
                height: 360.getHeight(context.width),
                width: 360.getWidth(context.width),
                decoration: const BoxDecoration(
                    shape: BoxShape.circle, color: Colors.white),
                child: SvgPicture.asset(
                  undraw_agreement_aajr,
                  // fit: BoxFit.cover,

                  height: 360,
                  width: 360,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

class DesktopHeaderBackgroundCurve extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    // Layer 1

    Paint paintFill0 = Paint()
      ..color = const Color.fromARGB(255, 255, 255, 255)
      ..style = PaintingStyle.fill
      ..strokeWidth = size.width * 0.00
      ..strokeCap = StrokeCap.butt
      ..strokeJoin = StrokeJoin.miter;
    paintFill0.shader = linearGradient(141, ["#EBF4FF", "#E6FFFA"])
        .createShader(Rect.fromCenter(
            center: Offset(size.width / 2, size.height / 2),
            width: size.width,
            height: size.height));

    Path path_1 = Path();
    path_1.moveTo(0, 0);
    path_1.lineTo(size.width * 0.9989583, 0);
    path_1.quadraticBezierTo(size.width * 1.0024688, size.height * 0.6730455,
        size.width * 1.0025990, size.height * 0.9105303);
    path_1.cubicTo(
        size.width * 0.6923125,
        size.height * 0.8685000,
        size.width * 0.3128906,
        size.height * 1.0268788,
        size.width * -0.0041667,
        size.height);
    path_1.quadraticBezierTo(
        size.width * -0.0031250, size.height * 0.7500000, 0, 0);
    path_1.close();

    canvas.drawPath(path_1, paintFill0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
