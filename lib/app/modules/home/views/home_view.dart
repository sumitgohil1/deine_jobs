import 'package:deine_jobs/app/modules/home/views/desktop/header/desktop_header.dart';
import 'package:deine_jobs/app/modules/home/views/mobile/body/mobile_body_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../../../../widgets/bottom_registration_widget.dart';
import '../../../../widgets/sliver_app.dart';
import '../controllers/home_controller.dart';
import 'desktop/body/desktop_body_widget.dart';
import 'mobile/header/mobile_header.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      extendBody: true,
      extendBodyBehindAppBar: true,
      bottomNavigationBar: const BottomRegistrationButton(),
      appBar: MyCustomAppBar(
        key: UniqueKey(),
        height: 72,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.only(top: 0),
          child: Column(
            children: [
              ScreenTypeLayout.builder(
                desktop: (_) => const DesktopHeader(),
                tablet: (_) => const DesktopHeader(),
                mobile: (_) => const MobileHeader(),
              ),
              ScreenTypeLayout.builder(
                desktop: (_) => const DesktopBodyWidget(),
                tablet: (_) => const DesktopBodyWidget(),
                mobile: (_) => ScreenUtilInit(
                    designSize: Size(360, 640),
                    builder: (_, child) {
                      return const MobileBodyWidget();
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
