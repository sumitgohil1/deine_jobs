extension MobileSize on int {
  double getHeightMobile(double currentHeight) => (this * currentHeight) / 640;
  double getWidthMobile(double currentWidth) => (this * currentWidth) / 360;
}
