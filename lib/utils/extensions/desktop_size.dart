extension DesktopSize on int {
  double getHeight(double currentHeight) => (this * currentHeight) / 1920;
  double getWidth(double currentWidth) => (this * currentWidth) / 1920;
}
