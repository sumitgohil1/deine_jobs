const String undraw_Profile_data_re_v81r =
    "assets/images/desktop/undraw_Profile_data_re_v81r.svg";
const String undraw_agreement_aajr =
    "assets/images/desktop/undraw_agreement_aajr.svg";
const String undraw_task_31wc = "assets/images/desktop/undraw_task_31wc.svg";
const String undraw_personal_file_222m =
    "assets/images/desktop/undraw_personal_file_222m.svg";
const String undraw_about_me_wa29 =
    "assets/images/desktop/undraw_about_me_wa29.svg";
const String undraw_swipe_profiles1_i6mr =
    "assets/images/desktop/undraw_swipe_profiles1_i6mr.svg";
const String undraw_business_deal_cpi9 =
    "assets/images/desktop/undraw_business_deal_cpi9.svg";

const String undraw_job_offers_kw5d =
    "assets/images/desktop/undraw_job_offers_kw5d.svg";

const String arrow1 = "assets/images/desktop/arrow1.svg";

const String arrow2 = "assets/images/desktop/arrow2.svg";

///// MOBILE ASSETS. Ideally it should remain the same but when I checked the internal code of both SVG from Desktop and Mobile view, they seem to be different. Thus using different SVG for both.
///

const String undraw_Profile_data_re_v81r_mobile =
    "assets/images/mobile/undraw_Profile_data_re_v81r_mobile.svg";

const String undraw_task_31wc_mobile =
    "assets/images/mobile/undraw_task_31wc_mobile.svg";
const String undraw_personal_file_222m_mobile =
    "assets/images/mobile/undraw_personal_file_222m_mobile.svg";
const String undraw_about_me_wa29_mobile =
    "assets/images/mobile/undraw_about_me_wa29_mobile.svg";
const String undraw_swipe_profiles1_i6mr_mobile =
    "assets/images/mobile/undraw_swipe_profiles1_i6mr_mobile.svg";
const String undraw_business_deal_cpi9_mobile =
    "assets/images/mobile/undraw_business_deal_cpi9_mobile.svg";

const String undraw_job_offers_kw5d_mobile =
    "assets/images/mobile/undraw_job_offers_kw5d_mobile.svg";
