import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../app/modules/home/controllers/home_controller.dart';

class CustomTabs extends GetView<HomeController> {
  const CustomTabs({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 600,
      height: 40,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: const Color(0xffCBD5E0)),
        color: Colors.white,
      ),
      child: ClipRRect(
          clipBehavior: Clip.antiAlias,
          borderRadius: const BorderRadius.all(
            Radius.circular(10.0),
          ),
          child: Obx(() => TabBar(
                  padding: EdgeInsets.all(0),
                  labelPadding: EdgeInsets.all(0),
                  indicator: BoxDecoration(
                    color: Color(0xff81E6D9),
                  ),
                  isScrollable: true,
                  dividerColor: Colors.pink,
                  controller: controller.tabController,
                  tabs: [
                    Tab(
                      child: SizedBox(
                        width: 200,
                        child: Center(
                          child: Text(
                            "Arbeitnehmer",
                            style: GoogleFonts.lato(
                              fontWeight: FontWeight.w600,
                              color: controller.selectedIndex.value == 0
                                  ? const Color(0xffE6FFFA)
                                  : const Color(0xff319795),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Tab(
                      child: Container(
                        width: 200,
                        decoration: const BoxDecoration(
                          border: Border.symmetric(
                            vertical:
                                BorderSide(color: Color(0xffCBD5E0), width: 1),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            "Arbeitnehmer",
                            style: GoogleFonts.lato(
                              fontWeight: FontWeight.w600,
                              color: controller.selectedIndex.value == 1
                                  ? const Color(0xffE6FFFA)
                                  : const Color(0xff319795),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Tab(
                      child: SizedBox(
                        width: 200,
                        child: Center(
                          child: Text(
                            "Arbeitnehmer",
                            style: GoogleFonts.lato(
                              fontWeight: FontWeight.w600,
                              color: controller.selectedIndex.value == 2
                                  ? const Color(0xffE6FFFA)
                                  : const Color(0xff319795),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ]))),
    );
  }
}
