import 'package:deine_jobs/utils/extensions/desktop_size.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_builder/responsive_builder.dart';

class TopRegistrationButton extends StatefulWidget {
  final double width;
  const TopRegistrationButton({
    super.key,
    required this.width,
  });

  @override
  State<TopRegistrationButton> createState() => _TopRegistrationButtonState();
}

class _TopRegistrationButtonState extends State<TopRegistrationButton> {
  bool isHovered = false;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      onHover: (hover) => setState(() {
        isHovered = hover;
      }),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20.2),
            width: widget.width,
            height: 40,
            decoration: BoxDecoration(
              border: Border.all(width: 1, color: const Color(0xffCBD5E0)),
              borderRadius: BorderRadius.circular(12),
              color: !isHovered ? Colors.white : const Color(0xffE6FFFA),
            ),
            child: Center(
              child: ScreenTypeLayout.builder(
                mobile: (context) => Text(
                  "Kostenlos Registrieren",
                  style: GoogleFonts.lato(
                      fontSize: 17,
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffE6FFFA)),
                ),
                tablet: (context) => Text(
                  "Kostenlos Registrieren",
                  style: GoogleFonts.lato(
                      fontSize: 21.getWidth(context.width),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xff319795)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
