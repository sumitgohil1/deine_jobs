import 'package:deine_jobs/utils/extensions/desktop_size.dart';
import 'package:deine_jobs/widgets/top_registration_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_builder/responsive_builder.dart';

import '../app/modules/home/controllers/home_controller.dart';

class AppBarLinksWidget extends GetView<HomeController> {
  const AppBarLinksWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      tablet: (context) => Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Obx(
            () => controller.isRegisterButtonVisible.isTrue
                ? TopRegistrationButton(
                    width: 255.getWidth(context.width),
                  )
                : SizedBox(),
          ),
          InkWell(
            onTap: () {},
            onHover: (isHovered) {
              controller.isLoginHovered.value = isHovered;
            },
            child: Obx(() {
              return Text(
                "Login",
                style: GoogleFonts.lato(
                    fontWeight: FontWeight.w600,
                    decoration: controller.isLoginHovered.isTrue
                        ? TextDecoration.underline
                        : TextDecoration.none,
                    fontSize: 14,
                    color: const Color(0xff319795)),
              );
            }),
          )
        ],
      ),
      mobile: (context) => Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          InkWell(
            onTap: () {},
            onHover: (isHovered) {
              controller.isLoginHovered.value = isHovered;
            },
            child: Obx(() {
              return Text(
                "Login",
                style: GoogleFonts.lato(
                    fontWeight: FontWeight.w600,
                    decoration: controller.isLoginHovered.isTrue
                        ? TextDecoration.underline
                        : TextDecoration.none,
                    fontSize: 14,
                    color: const Color(0xff319795)),
              );
            }),
          )
        ],
      ),
    );
  }
}
