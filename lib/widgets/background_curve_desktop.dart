import 'package:flutter/material.dart';
import 'package:gradient_like_css/gradient_like_css.dart';

//Copy this CustomPainter code to the Bottom of the File
class BackgroundCurveDesktop extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.9999305, size.height * 0.2420922);
    path_0.arcToPoint(Offset(size.width * 0.9982060, size.height * 0.2270229),
        radius: Radius.elliptical(
            size.width * 0.004067867, size.height * 0.02129363),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.cubicTo(
        size.width * 0.9833252,
        size.height * 0.1780657,
        size.width * 0.9676100,
        size.height * 0.1376988,
        size.width * 0.9508310,
        size.height * 0.1101809);
    path_0.arcToPoint(Offset(size.width * 0.9061122, size.height * 0.05689222),
        radius:
            Radius.elliptical(size.width * 0.2688408, size.height * 1.407273),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.cubicTo(
        size.width * 0.8942911,
        size.height * 0.04862956,
        size.width * 0.8825464,
        size.height * 0.03912933,
        size.width * 0.8707253,
        size.height * 0.03214065);
    path_0.cubicTo(
        size.width * 0.8631875,
        size.height * 0.02769992,
        size.width * 0.8555664,
        size.height * 0.02751793,
        size.width * 0.8480008,
        size.height * 0.02420558);
    path_0.cubicTo(
        size.width * 0.8162784,
        size.height * 0.01022822,
        size.width * 0.7844795,
        size.height * 0.01259418,
        size.width * 0.7527362,
        size.height * 0.01354057);
    path_0.cubicTo(
        size.width * 0.7362214,
        size.height * 0.01405016,
        size.width * 0.7197066,
        size.height * 0.01801769,
        size.width * 0.7031987,
        size.height * 0.02082044);
    path_0.cubicTo(
        size.width * 0.6938321,
        size.height * 0.02264041,
        size.width * 0.6845004,
        size.height * 0.02522477,
        size.width * 0.6751964,
        size.height * 0.02784552);
    path_0.quadraticBezierTo(size.width * 0.6569015, size.height * 0.03297783,
        size.width * 0.6386065, size.height * 0.03876533);
    path_0.cubicTo(
        size.width * 0.6281413,
        size.height * 0.04211408,
        size.width * 0.6176831,
        size.height * 0.04604521,
        size.width * 0.6072318,
        size.height * 0.04968515);
    path_0.cubicTo(
        size.width * 0.5906752,
        size.height * 0.05558184,
        size.width * 0.5741256,
        size.height * 0.06176974,
        size.width * 0.5575760,
        size.height * 0.06759364);
    path_0.cubicTo(
        size.width * 0.5468743,
        size.height * 0.07123357,
        size.width * 0.5361727,
        size.height * 0.07581990,
        size.width * 0.5254642,
        size.height * 0.07811306);
    path_0.quadraticBezierTo(size.width * 0.4922050, size.height * 0.08539293,
        size.width * 0.4589389, size.height * 0.09005205);
    path_0.cubicTo(
        size.width * 0.4482094,
        size.height * 0.09158082,
        size.width * 0.4374731,
        size.height * 0.09088924,
        size.width * 0.4267367,
        size.height * 0.09005205);
    path_0.cubicTo(
        size.width * 0.4143384,
        size.height * 0.08910567,
        size.width * 0.4019262,
        size.height * 0.08808648,
        size.width * 0.3895418,
        size.height * 0.08506534);
    path_0.cubicTo(
        size.width * 0.3757249,
        size.height * 0.08164380,
        size.width * 0.3619220,
        size.height * 0.07603829,
        size.width * 0.3481190,
        size.height * 0.07126997);
    path_0.cubicTo(
        size.width * 0.3369515,
        size.height * 0.06741164,
        size.width * 0.3257771,
        size.height * 0.06399010,
        size.width * 0.3146235,
        size.height * 0.05936738);
    path_0.cubicTo(
        size.width * 0.3021069,
        size.height * 0.05427147,
        size.width * 0.2895904,
        size.height * 0.04793798,
        size.width * 0.2770739,
        size.height * 0.04244167);
    path_0.cubicTo(
        size.width * 0.2674153,
        size.height * 0.03821934,
        size.width * 0.2577498,
        size.height * 0.03461580,
        size.width * 0.2480982,
        size.height * 0.03046628);
    path_0.cubicTo(
        size.width * 0.2356860,
        size.height * 0.02511557,
        size.width * 0.2232946,
        size.height * 0.01892767,
        size.width * 0.2108824,
        size.height * 0.01408656);
    path_0.cubicTo(
        size.width * 0.2013212,
        size.height * 0.01044662,
        size.width * 0.1917461,
        size.height * 0.006588287,
        size.width * 0.1821709,
        size.height * 0.005496305);
    path_0.cubicTo(
        size.width * 0.1573882,
        size.height * 0.002766352,
        size.width * 0.1326264,
        size.height * -0.0009099843,
        size.width * 0.1078785,
        size.height * 0.0002183962);
    path_0.cubicTo(
        size.width * 0.08499409,
        size.height * 0.001237579,
        size.width * 0.06210973,
        size.height * 0.006770284,
        size.width * 0.03924623,
        size.height * 0.01270338);
    path_0.cubicTo(
        size.width * 0.02715388,
        size.height * 0.01583373,
        size.width * 0.01511021,
        size.height * 0.02402359,
        size.width * 0.003045685,
        size.height * 0.02981109);
    path_0.arcToPoint(
        Offset(size.width * 0.0006953619, size.height * 0.02911950),
        radius: Radius.elliptical(
            size.width * 0.01866351, size.height * 0.09769592),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.0006953619, size.height * 0.02860991);
    path_0.arcToPoint(Offset(0, size.height * 0.02860991),
        radius: Radius.elliptical(
            size.width * 0.005764550, size.height * 0.03017508),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(0, size.height * 0.09412878);
    path_0.quadraticBezierTo(
        0, size.height * 0.5469370, 0, size.height * 0.9997088);
    path_0.arcToPoint(Offset(size.width * 0.0006953619, size.height * 1.000036),
        radius: Radius.elliptical(
            size.width * 0.005681107, size.height * 0.02973829),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.0006953619, size.height * 0.9997816);
    path_0.cubicTo(
        size.width * 0.002301648,
        size.height * 0.9997816,
        size.width * 0.003546346,
        size.height * 0.9997816,
        size.width * 0.004791044,
        size.height * 0.9997816);
    path_0.cubicTo(
        size.width * 0.01959530,
        size.height * 0.9980708,
        size.width * 0.03440651,
        size.height * 0.9966149,
        size.width * 0.04921076,
        size.height * 0.9944673);
    path_0.cubicTo(
        size.width * 0.06249913,
        size.height * 0.9925381,
        size.width * 0.07578054,
        size.height * 0.9899902,
        size.width * 0.08906196,
        size.height * 0.9873694);
    path_0.quadraticBezierTo(size.width * 0.1153884, size.height * 0.9821279,
        size.width * 0.1417078, size.height * 0.9764496);
    path_0.cubicTo(
        size.width * 0.1570614,
        size.height * 0.9732465,
        size.width * 0.1724219,
        size.height * 0.9702981,
        size.width * 0.1877686,
        size.height * 0.9666946);
    path_0.cubicTo(
        size.width * 0.2066963,
        size.height * 0.9622174,
        size.width * 0.2256102,
        size.height * 0.9568667,
        size.width * 0.2445379,
        size.height * 0.9524624);
    path_0.cubicTo(
        size.width * 0.2544747,
        size.height * 0.9501693,
        size.width * 0.2644183,
        size.height * 0.9491865,
        size.width * 0.2743551,
        size.height * 0.9474393);
    path_0.cubicTo(
        size.width * 0.2878590,
        size.height * 0.9450369,
        size.width * 0.3013629,
        size.height * 0.9412514,
        size.width * 0.3148738,
        size.height * 0.9401594);
    path_0.quadraticBezierTo(size.width * 0.3735345, size.height * 0.9360099,
        size.width * 0.4322022, size.height * 0.9338623);
    path_0.cubicTo(
        size.width * 0.4454975,
        size.height * 0.9333891,
        size.width * 0.4587998,
        size.height * 0.9359371,
        size.width * 0.4721021,
        size.height * 0.9372475);
    path_0.cubicTo(
        size.width * 0.4834365,
        size.height * 0.9383759,
        size.width * 0.4947639,
        size.height * 0.9398682,
        size.width * 0.5060983,
        size.height * 0.9408874);
    path_0.cubicTo(
        size.width * 0.5221751,
        size.height * 0.9421978,
        size.width * 0.5382519,
        size.height * 0.9429986,
        size.width * 0.5543286,
        size.height * 0.9443090);
    path_0.cubicTo(
        size.width * 0.5582435,
        size.height * 0.9446366,
        size.width * 0.5621584,
        size.height * 0.9472573,
        size.width * 0.5660594,
        size.height * 0.9466385);
    path_0.cubicTo(
        size.width * 0.5765524,
        size.height * 0.9450005,
        size.width * 0.5870385,
        size.height * 0.9417610,
        size.width * 0.5975315,
        size.height * 0.9401230);
    path_0.cubicTo(
        size.width * 0.6002642,
        size.height * 0.9396862,
        size.width * 0.6030248,
        size.height * 0.9441634,
        size.width * 0.6057715,
        size.height * 0.9439814);
    path_0.quadraticBezierTo(size.width * 0.6227661, size.height * 0.9427438,
        size.width * 0.6397330, size.height * 0.9400866);
    path_0.quadraticBezierTo(size.width * 0.6559905, size.height * 0.9373931,
        size.width * 0.6722412, size.height * 0.9331707);
    path_0.quadraticBezierTo(size.width * 0.6917600, size.height * 0.9281476,
        size.width * 0.7112649, size.height * 0.9220325);
    path_0.cubicTo(
        size.width * 0.7215006,
        size.height * 0.9188294,
        size.width * 0.7317294,
        size.height * 0.9154079,
        size.width * 0.7419512,
        size.height * 0.9112947);
    path_0.quadraticBezierTo(size.width * 0.7634587, size.height * 0.9027045,
        size.width * 0.7849454, size.height * 0.8928766);
    path_0.cubicTo(
        size.width * 0.7968291,
        size.height * 0.8874167,
        size.width * 0.8087129,
        size.height * 0.8815564,
        size.width * 0.8205688,
        size.height * 0.8746770);
    path_0.cubicTo(
        size.width * 0.8363883,
        size.height * 0.8656135,
        size.width * 0.8522286,
        size.height * 0.8568413,
        size.width * 0.8679855,
        size.height * 0.8455575);
    path_0.cubicTo(
        size.width * 0.8875600,
        size.height * 0.8313981,
        size.width * 0.9071622,
        size.height * 0.8172023,
        size.width * 0.9265767,
        size.height * 0.7982383);
    path_0.cubicTo(
        size.width * 0.9458591,
        size.height * 0.7794926,
        size.width * 0.9649607,
        size.height * 0.7556510,
        size.width * 0.9840623,
        size.height * 0.7323190);
    path_0.cubicTo(
        size.width * 0.9890272,
        size.height * 0.7262403,
        size.width * 0.9937278,
        size.height * 0.7141193,
        size.width * 0.9984633,
        size.height * 0.7036363);
    path_0.cubicTo(
        size.width * 0.9992073,
        size.height * 0.7019983,
        size.width * 0.9999305,
        size.height * 0.6947912,
        size.width * 0.9999305,
        size.height * 0.6901321);
    path_0.quadraticBezierTo(size.width * 1.000063, size.height * 0.4661668,
        size.width * 0.9999305, size.height * 0.2420922);
    path_0.close();

    // Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    // paint0Fill.color = const Color(0xff000000).withOpacity(1.0);

    Paint paint0Fill = Paint()
      ..color = const Color.fromARGB(255, 255, 255, 255)
      ..style = PaintingStyle.fill
      ..strokeWidth = size.width * 0.00
      ..strokeCap = StrokeCap.butt
      ..strokeJoin = StrokeJoin.miter;
    paint0Fill.shader = linearGradient(104, ["#E6FFFA", "#EBF4FF"])
        .createShader(Rect.fromCenter(
            center: Offset(size.width / 2, size.height / 2),
            width: size.width,
            height: size.height));

    canvas.drawPath(path_0, paint0Fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
