import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'registration_button.dart';

class BottomRegistrationButton extends StatelessWidget {
  const BottomRegistrationButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      desktop: (_) => const SizedBox(),
      tablet: (_) => const SizedBox(),
      mobile: (context) => Container(
        width: context.width,
        height: 80,
        decoration: const BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color(0x33000000),
              offset: Offset(0, -1),
              blurRadius: 3,
            ),
          ],
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12),
            topRight: Radius.circular(12),
          ),
        ),
        child: RegustrationButton(width: context.width),
      ),
    );
  }
}
