import 'package:deine_jobs/utils/extensions/desktop_size.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'gradient_container.dart';

class RegustrationButton extends StatefulWidget {
  final double width;
  const RegustrationButton({
    super.key,
    required this.width,
  });

  @override
  State<RegustrationButton> createState() => _RegustrationButtonState();
}

class _RegustrationButtonState extends State<RegustrationButton> {
  bool isHovered = false;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      onHover: (hover) => setState(() {
        isHovered = hover;
      }),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20.2),
            width: widget.width,
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              gradient: isHovered
                  ? const ContainerLinearGradient(95, ['#2C7A7B', '#2B6CB0'])
                      .getLinearGradient()
                  : const ContainerLinearGradient(95, ['#319795', '#3182CE'])
                      .getLinearGradient(),
            ),
            child: Center(
              child: ScreenTypeLayout.builder(
                mobile: (context) => Text(
                  "Kostenlos Registrieren",
                  style: GoogleFonts.lato(
                      fontSize: 17,
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffE6FFFA)),
                ),
                tablet: (context) => Text(
                  "Kostenlos Registrieren",
                  style: GoogleFonts.lato(
                      fontSize: 21.getWidth(context.width),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffE6FFFA)),
                ),
                desktop: (context) => Text(
                  "Kostenlos Registrieren",
                  style: GoogleFonts.lato(
                      fontSize: 17.getWidth(context.width),
                      fontWeight: FontWeight.w400,
                      color: const Color(0xffE6FFFA)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
