import 'package:flutter/material.dart';
import 'package:gradient_like_css/gradient_like_css.dart';

//Copy this CustomPainter code to the Bottom of the File
class BackgroundCurveMobile extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 1.002778, size.height * 0.02432432);
    path_0.lineTo(size.width * 1.002778, size.height * 0.8621622);
    path_0.arcToPoint(Offset(size.width * 0.9934167, size.height * 0.8675676),
        radius: Radius.elliptical(
            size.width * 0.05513889, size.height * 0.05364865),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.cubicTo(
        size.width * 0.9595278,
        size.height * 0.8797568,
        size.width * 0.9259444,
        size.height * 0.8928108,
        size.width * 0.8915833,
        size.height * 0.9035405);
    path_0.cubicTo(
        size.width * 0.8126111,
        size.height * 0.9281892,
        size.width * 0.7317500,
        size.height * 0.9447838,
        size.width * 0.6486667,
        size.height * 0.9453784);
    path_0.cubicTo(
        size.width * 0.5966667,
        size.height * 0.9457297,
        size.width * 0.5446389,
        size.height * 0.9401622,
        size.width * 0.4925833,
        size.height * 0.9381892);
    path_0.cubicTo(
        size.width * 0.4451389,
        size.height * 0.9363784,
        size.width * 0.3975833,
        size.height * 0.9340000,
        size.width * 0.3501667,
        size.height * 0.9353243);
    path_0.arcToPoint(Offset(size.width * 0.2251667, size.height * 0.9465676),
        radius:
            Radius.elliptical(size.width * 1.149028, size.height * 1.117973),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.cubicTo(
        size.width * 0.1615833,
        size.height * 0.9551892,
        size.width * 0.09830556,
        size.height * 0.9659189,
        size.width * 0.03497222,
        size.height * 0.9761081);
    path_0.cubicTo(
        size.width * 0.02597222,
        size.height * 0.9776757,
        size.width * 0.01722222,
        size.height * 0.9812162,
        size.width * 0.008333333,
        size.height * 0.9837838);
    path_0.lineTo(0, size.height * 0.9837838);
    path_0.quadraticBezierTo(0, size.height * 0.4918919, 0, 0);
    path_0.lineTo(size.width * 0.04444444, 0);
    path_0.arcToPoint(
        Offset(size.width * 0.05458333, size.height * 0.002540541),
        radius:
            Radius.elliptical(size.width * 0.1025278, size.height * 0.09975676),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.cubicTo(
        size.width * 0.07750000,
        size.height * 0.006000000,
        size.width * 0.1010556,
        size.height * 0.007189189,
        size.width * 0.1232222,
        size.height * 0.01316216);
    path_0.cubicTo(
        size.width * 0.1683611,
        size.height * 0.02532432,
        size.width * 0.2127778,
        size.height * 0.04000000,
        size.width * 0.2574444,
        size.height * 0.05370270);
    path_0.cubicTo(
        size.width * 0.3133056,
        size.height * 0.07091892,
        size.width * 0.3696667,
        size.height * 0.08648649,
        size.width * 0.4282778,
        size.height * 0.09121622);
    path_0.cubicTo(
        size.width * 0.4796111,
        size.height * 0.09532432,
        size.width * 0.5302778,
        size.height * 0.08881081,
        size.width * 0.5800833,
        size.height * 0.07678378);
    path_0.cubicTo(
        size.width * 0.6679167,
        size.height * 0.05556757,
        size.width * 0.7540278,
        size.height * 0.02637838,
        size.width * 0.8446944,
        size.height * 0.01848649);
    path_0.cubicTo(
        size.width * 0.8884444,
        size.height * 0.01467568,
        size.width * 0.9328056,
        size.height * 0.01659459,
        size.width * 0.9768889,
        size.height * 0.01694595);
    path_0.cubicTo(
        size.width * 0.9855278,
        size.height * 0.01705405,
        size.width * 0.9941389,
        size.height * 0.02175676,
        size.width * 1.002778,
        size.height * 0.02432432);
    path_0.close();

    Paint paint0Fill = Paint()
      ..color = const Color.fromARGB(255, 255, 255, 255)
      ..style = PaintingStyle.fill
      ..strokeWidth = size.width * 0.00
      ..strokeCap = StrokeCap.butt
      ..strokeJoin = StrokeJoin.miter;
    paint0Fill.shader = linearGradient(134, ["#E6FFFA", "#EBF4FF"])
        .createShader(Rect.fromCenter(
            center: Offset(size.width / 2, size.height / 2),
            width: size.width,
            height: size.height));

    canvas.drawPath(path_0, paint0Fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
