import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'app_bar_links.dart';
import 'gradient_container.dart';

class MyCustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final double height;

  const MyCustomAppBar({
    required Key key,
    required this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 5,
          width: context.width,
          decoration: BoxDecoration(
              gradient:
                  const ContainerLinearGradient(95, ['#319795', '#3182CE'])
                      .getLinearGradient()),
        ),
        Container(
          width: context.width,
          height: 67,
          padding: const EdgeInsets.symmetric(horizontal: 19),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(12),
              bottomRight: Radius.circular(12),
            ),
            boxShadow: [
              BoxShadow(
                color: Color(0x29000000),
                offset: Offset(0, 3),
                blurRadius: 6,
              ),
            ],
          ),
          child: const AppBarLinksWidget(),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}
