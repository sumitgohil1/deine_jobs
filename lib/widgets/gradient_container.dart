// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:gradient_like_css/gradient_like_css.dart';

class ContainerLinearGradient extends LinearGradient {
  final int gradientAngle;
  final List<String> gradientColors;

  const ContainerLinearGradient(this.gradientAngle, this.gradientColors,
      {super.colors = const []});

  getLinearGradient() {
    return linearGradient(gradientAngle, gradientColors);
  }
}
